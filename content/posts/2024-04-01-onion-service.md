---
title: "성소수자 불법화에 따른 우회 접속법 안내"
<!-- date: 2024-03-13 23:34:52 +0900 -->
date: 2024-04-01T09:00:00+09:00
lastmod: 2024-03-13 23:46:34 +0900
authors:
- jarm
categories:
- joke
tags:
- 만우절
- tor
---

안녕하세요, 큐돈입니다. 2024년 4월 1일 오늘, 성소수자를 처벌하는 법안이 발의되었습니다. 이에 따라 성소수자 불법색출이 이루어지고 있기 때문에 성소수자 테마를 가지고 있는 큐돈도 가만히 앉아 있을 수는 없습니다.
현재 대한민국은 SNI 검열이 이루어져 국가에서 국민들이 어떤 웹사이트에 접근하는지를 감시할 수 있는 상황이기 때문에 큐돈에 아무런 보호장치 없이 접근하는 것은 수색영장을 받을 수도 있게 되었습니다. 이에 따라 Tor 히든 서비스 onion 주소를 제공하게 되었습니다.


## Tor Hidden Service란

Tor는 The Onion Router의 약자로, 목적지에 바로 메시지를 전송하는 것이 아니라 메시지를 겹겹이 암호화 하여 세계 각지에 떨어져 있는 자원봉사자들의 컴퓨터를 거쳐 접속해 중간의 어디에서도 접속자의 정체나 메시지 내용 모두를 한 번에 알 수 없도록 해 주는 통신 프로토콜입니다. 검열이 강하게 작용하는 국가 등에서 검열을 피해 진실을 알리려는 기자들도 자주 사용하는 기술입니다.

하지만 Tor도 마지막 노드에서는 메시지의 내용을 볼 수 있고(HTTPS로 암호화 되어서 의미가 없긴 합니다), DNS 요청은 암호화가 되지 않아 Tor 노드를 경찰이 운영하고 있다면(보통 Tor는 이 상황을 피하기 위해 세계 곳곳의 노드를 골고루 사용하려 합니다) 어느 정도 정보가 노출이 됩니다. 그래서 Tor 안에서만 메시지를 주고 받는 onion 히든 서비스가 등장하게 됩니다. 도메인 주소가 `.onion`으로 끝나는 주소인 경우 Tor를 통해서만 접근할 수 있고 누가 어디서 여기에 접근하는지 쉽게 알 수 있는 방법이 없습니다.


## 접속 방법

앞서 설명한대로 onion 히든 서비스로 접근하기 위해서는 Tor 브라우저가 필요합니다.
Tor 브라우저는 Tor project의 [다운로드 페이지](https://www.torproject.org/download/)에서 다운로드 받을 수 있습니다.

다운로드 받은 Tor 브라우저는 일반 브라우저와 사용법이 크게 다르지 않습니다. 별다른 설정을 할 것 없이 바로 Tor 망을 사용하여 인터넷에 접속할 수 있습니다.

주소창에 http://nqt42rzz5ybtslld3yvfv7orovscbfpylv2aaybqiri6cpql3kxdcpad.onion/ 를 입력합니다. https가 아님에 유의합니다. (Tor로 이미 암호화가 되어 있고 onion 주소 자체가 전자서명과 마찬가지이기 때문에 추가적인 TLS를 사용하지 않습니다)

{{<figure title="Qdon with onion hidden service" caption="Onion 히든 서비스 큐돈에 접속한 스크린샷" src="/img/2024-04-01/tor-qdon-onion.png">}}

혹은 그냥 qdon.space를 입력하고 접속할 수도 있습니다 (앞서 설명한대로 해외의 마지막 노드에선 qdon.space 주소를 얻어오는 DNS 요청은 볼 수 있습니다). 이 경우엔 주소창 오른쪽에 *.onion available*이라는 보라색 버튼이 뜨며 이 버튼을 누르면 위의 복잡한 onion 주소로 접속할 수 있습니다.

{{<figure title="Qdon with Tor browser" caption="Tor 브라우저를 통해 큐돈에 접속한 스크린샷" src="/img/2024-04-01/tor-qdon.png">}}

위 방법을 통해 안전하게 국가의 검열을 피해 큐돈에 접속하실 수 있습니다.


## 국가에서 Tor마저 검열하는 경우

Tor가 검열을 피하기 위해 사용되는만큼 Tor 사용 자체를 근거로 수색영장을 받을 수도 있습니다. 이 때는 Tor 사용 자체를 숨길 수 있는 방법을 사용해야 합니다. Tor는 이런 경우에 사용할 대비책도 미리 가지고 있습니다.

Tor 브라우저의 설정 페이지에 들어가 Bridges 항목을 살펴봅니다. 브리지는 Tor로 연결하기 위한 또 하나의 다리가 되어주며, Tor로 오고가는 메시지를 일반적인 인터넷 사용을 하는 것처럼 속여줍니다.

{{<figure title="Tor browser bridge setting" caption="Tor 브라우저의 설정화면의 Bridges 항목" src="/img/2024-04-01/tor-settings-bridges.png">}}

브리지엔 여러가지 옵션이 있는데 *obfs4*가 기본값이지만 만약 검열이 강화된다면 *meek-azure*를 사용해 MS azure 클라우드 서비스에 접속하는 것처럼 속일 수 있습니다.

{{<figure title="Tor browser bridge setting - type" caption="Tor 브라우저의 설정화면의 브리지 종류를 고르는 화면" src="/img/2024-04-01/tor-settings-bridges-type.png">}}

위 방법을 사용해 성소수자 탄압이 시작된 대한민국에서도 큐돈에 접속하실 수 있습니다.

<div style="height: 150vh;"></div>

오늘은 4월 1일 만우절입니다. 성소수자를 불법화 하는 법안은 공포되지도 발의되지도 않았습니다 (적어도 이 글을 쓰는 3월 13일 기준으로는요).
하지만 언젠가 정말로 국가에서 큐돈 접속을 불법화 하는 날이 절대로 오지 않는다고는 아무도 보장할 수 없습니다. 그래서 큐돈은 2022년 6월부터 Tor hidden service 주소를 제공[^1]하고 있습니다. 이 글에 나와 있는 방법은 실제로 사용할 수 있는 방법입니다. 다만 어뷰징을 막기 위해 Tor 히든 주소를 통해 접근한 경우엔 가입이 승인제로 전환되어 확인을 거친 후에만 가입할 수 있습니다.

만우절 농담글 치고는 다소 진지한 내용을 담아보았습니다. 현재 대한민국은 SNI 검열을 통해 국민들이 어느 사이트에 접속하고 있는지를 감시하고 있다는 부분은 농담이 아닌 현실입니다. 현재는 불법 웹사이트들을 차단한다는 명목하에 운영되고 있지만 이 기술을 뒤에서 어떻게 쓰고 있을지는 아무도 모르는 일이며 실제로 나쁜 방향으로 쓸 수 있는 가능성이 있다는 것은 명백한 사실입니다. 한번쯤은 고민해 보아야 할 문제입니다.

[^1]: [{{<relref "2022-06-11-onion-service.md">}}]({{<relref "2022-06-11-onion-service.md">}})
