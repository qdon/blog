---
title: "큐돈의 이미지들"
authors:
- jarm
date: 2019-04-06 00:30:17 +0900
lastmod: 2019-06-06 00:40:56 +0900
categories:
- story
tags:
- Qdon
aliases:
- /posts/2019-04-06-assets-of-qdon/
---

큐돈엔 몇 가지 이미지들이 있습니다. 물론 다른 곳에도 없는 건 아닙니다. planet.moe의 {{<emoji src="/img/reni.png" name="reni">}}같은 마스코트가 대표적인 예시죠. 오늘은 큐돈의 대표적인 이미지들이 무엇이 있고, 어떻게 만들어 졌는 지 알아보도록 합니다.
<!--more-->

<center>
{{<figure title="Qdon" caption="큐돈의 로고" width="100px" src="/img/logo.png">}}
{{<figure title="Gyudon" caption="큐돈의 기본 프로필 사진: 규동" width="100px" src="/img/gyudon.png">}}
{{<figure title="Qnail" caption="큐돈의 마스코트: Qnail" width="100px" src="/img/snail-mascot.png">}}
{{<figure title="Header" caption="큐돈의 헤더 이미지" width="100%" src="/img/header.png">}}
</center>


## 로고

<img src="/img/logo.png" alt="Qdon logo" style="float: left; max-width: 50%; margin-right: 2em;" />

먼저 큐돈의 로고입니다.
성소수자 관련 플래그들의 색을 생각 나는대로 다 이어 붙였습니다.

- 6색 무지개
- 트랜스젠더
- 젠더퀴어
- A섹슈얼
- 팬섹슈얼

성소수자의 종류는 훨씬 더 많은데에 비해 몇 가지가 들어가지 않았습니다. \
아마 미디어에서 성소수자를 "동성애"라고 칭하는 거에 화가 난 마스터의 "우리도 성소수자다" 하는 마음이 드러난 것 같습니다. \
그래도 6색 무지개를 맨 앞에 뒀으니 괜찮다고 생각 합니다. 일부를 배제한 게 아니라 대표자로 몇 개만 나왔다고 생각하면 될 것 같습니다.

이 이미지는 놀랍게도 그래픽 툴로 만든 게 아니고 이 변태같은 마스터가 HTML로 만든 것입니다. 수치를 일괄적으로 수정하기에는 이 방식이 훨씬 편하거든요. (HTML 캔버스에 JS로 그림을 그리기도 합니다)

https://codepen.io/kjwon15/pen/VMWxwe

<hr style="clear: both; visibility: hidden;" />


## 프로필 사진 규동

<img src="/img/gyudon.png" alt="gyudon" style="float: right; max-width: 50%; margin-left: 2em;" />

당연하게도 Qdon의 말장난입니다. 이름을 지을 때부터 살짝 언어유희를 넣겠다는 의지가 있었죠.\
다만 제가 그림을 그릴 줄은 몰라서 그나마 할 줄 아는 도트 그래픽으로 규동을 그려 봤습니다.

이 이미지는 큐돈에 가입하면 기본 프로필 사진으로 사용 되는데 마스토돈은 기본적으로 기본 프사는 ActivityPub 프로토콜에 노출 시키지 않아 외부 인스턴스에서는 보이지 않게 됩니다. 이게 아쉬워서 큐돈에서는 자체 커스텀으로 기본 프로필 이미지도 ActivityPub에 노출 시키도록 하였습니다[^1]

큐돈 유저들이 모이면 규동을 먹는 풍습이 있다는 소문이 있으나, 큐돈 유저끼리 만난 적은 한 번도 없습니다.

이 이미지는 pixil이라는 사이트를 통해 만들었어요.웹에서 도트를 찍을 수 있는 툴을 제공합니다.
https://www.pixilart.com/art/gyudon-28c63db01c052b2

<hr style="clear: both; visibility: hidden;" />


## 마스코트 달팽이

<img src="/img/snail-mascot.png" alt="gyudon" style="float: left; max-width: 50%; margin-right: 2em;" />

큐돈의 마스코트가 달팽이로 정해진 이유는 딱히 없습니다. 그냥 Planet의 레니가 부러워 마스코트를 그려보고 싶었던 마스터가 마스코트로 그릴 만 한 걸 추천 받았는데 그게 달팽이였을 뿐입니다.

{{< mastodon width="650px" src="https://qdon.space/@jarm/101727011427140435" >}}

그런데 그리다 보니 달팽이 집에 Q 무늬가 나타나더라구요. 그래서 이름을 Qnail이라고 지었습니다.
아쉽게도 마스토돈 2.8.0부터는 메인 페이지에 마스코트가 나타나지 않게 되어서 웹앱의 왼쪽 아래에서만 볼 수 있는데 모바일용 웹앱은 또 마스코트를 숨겨 둬서 이 마스코트를 볼 수 있는 곳은 한정 되어 있습니다.

이것도 역시 Pixil을 이용했는데 그냥 그린 다음에 그걸 새로 불러와서 명암을 넣어버리는 바람에 포스트가 두 개로 분리되었어요.
https://www.pixilart.com/art/snail-6987e981b7e0466
https://www.pixilart.com/art/snail-8825fa0b731f1be

<hr style="clear: both; visibility: hidden;" />


## 헤더 이미지


<img src="/img/header.png" alt="header" style="float: right; max-width: 50%; margin-left: 2em;" />

큐돈의 메인 페이지에서도 보이지만 큐돈 링크를 외부에 공유할 때 주로 보이는 이미지입니다.
switter의 헤더 이미지를 참고해서 Gimp로 만들었어요.

위쪽엔 로고를 그대로 넣고 아래는 "Norwester Condensed" 폰트를 사용해 커닝을 적당히 넣고 그 밑의 설명은 "Noto Mono" 폰트를 사용했어요.

이 이미지도 `/about/more` 페이지에서 이미지 사이즈가 `cover`로 되어 있어서 잘리는 문제가 있어서 살짝 커스텀을 했어요.

```scss
// Header image on /about/more
.public-layout .column-0 .public-account-header__image {
  background: #6494ed;

  img {
    object-fit: contain;
  }
}
```

이미지 사이즈를 `contain`으로 바꾸고 남는 여백을 배경과 동일한 색으로 채워버리는 눈속임입니다.

이 헤더엔 부끄러운 역사가 하나 있는데 예전엔 friendly가 오타가 나서 frendly였나 그런 식으로 적혀 있었어요. 다행히 아무도 못 본 것 같지만요. 소스 파일을 저장해 두는 게 중요한 것 같아요. 안 그랬으면 처음부터 다시 만들어야 했을테니까요.

<hr style="clear: both; visibility: hidden;" />

[^1]: https://bitbucket.org/qdon/qdon.space/commits/f5ca511abcab3c82d18eabfd481559ff7ecbb331#Lapp/serializers/activitypub/actor_serializer.rbT32
