---
title: "서버 상태 감시하기"
authors:
- jarm
date: 2019-06-03 15:41:37 +0900
categories:
- tech
tags:
- sysop
---


Github나 Twitter 같은 대형 서비스들은 서비스가 언제 장애가 났는 지 확인하기 위한 Status 페이지들이 있습니다.
마스토돈에선 어떻게 이를 설정하는 지 알아봅니다.

<!--more-->

일단 Github의 [상태 페이지](https://www.githubstatus.com/)를 봅시다. 각 기능별로 제대로 돌아가고 있는지를 우선 보여주고 최근의 장애가 언제 일어났는지를 보여주고 있습니다. 이를 참고해 상태 페이지에 무엇을 나타낼 지 생각해 봅시다.


## 감시 할 엔드포인트들

### 메인 페이지

`/about/more/` 혹은 `/about/` 페이지를 감시하여 메인 페이지가 제대로 뜨나 감시합시다.
로그인 된 유저들의 웹 UI는 잘 뜨는 와중에 이 페이지들이 뜨지 않는 경우가 서버 관리자들에게서 종종 보고 됩니다. 이는 신규 유저의 유입에 장애가 되고 기존 유저도 새로 로그인을 해야 할 때 문제가 발생하기 때문에 감시 할 이유가 충분합니다.

저는 `/about/more/` 페이지에 있는 관리자 핸들을 키워드로 잡아 감시하도록 했습니다.


### API

메인페이지가 박살나도 API만 제대로 동작하면 앱을 통한 이용은 가능합니다. 서버가 통채로 죽은 건지 아니면 웹 프론트단이 박살나서 500 에러가 뜨는 건지 알 필요가 있습니다. 실제로 웹팩이 고장나면 프론트엔드에서는 500 에러를 띄우지만 API는 제대로 동작하는 걸 몇 번 목격 했습니다.

API에서 대표적인 엔드포인트 `/api/v1/instance` 엔드포인트가 있는데 여기엔 `version` 엔트리가 있습니다. 이 키워드를 감시하도록 하겠습니다.


### 스트리밍 API

API이긴 하지만 마스토돈의 스트리밍 API는 루비가 아닌 노드JS를 이용해 다른 프로세스가 돌아갑니다. 따로 체크 해야 할 이유도 충분하고 이를 위해 `/api/v1/streaming/health`라는 좋은 엔드포인트도 제공 됩니다. "OK"가 뜨면 정상이기 때문에 이를 키워드로 잡습니다.

아쉽게도 [마스토돈의 API 문서](https://docs.joinmastodon.org/api/streaming/)에는 해당 엔드포인트에 대한 언급이 없네요. 제가 추가해야겠습니다.


### 유저 페이지

메인 페이지가 박살나면 같이 박살 날 확률이 매우 높지만 그래도 DB에서 설정만 가져오는 게 아닌, 유저의 프로필, 글 등을 가져오는 페이지이므로 저는 추가적으로 체크를 하도록 했습니다. 이를 체크하기 위해서는 항시 존재하는 유저가 있어야 하는데 관리자 계정이 그 용도로 딱이죠. [`/@qdon_support/`](https://qdon.space/@qdon_support) 페이지를 추가해 해당 유저의 핸들 `qdon_support@qdon.space`를 키워드로 설정 했습니다.


### 오브젝트 스토리지

마스토돈에서 이미지 파일 등의 미디어는 외부 스토리지를 통해 호스트 됩니다. 오브젝트 스토리지가 문제인지 서버 설정이 문제인지 알기 위해서 체크를 하기 위해 오브젝트 스토리지에 "check.txt"라는 파일을 만들고 내용은 `OK`라고 넣었습니다.


### ActivityPub

유저들은 알 필요가 없을 수도 있는 내용이지만 마스토돈을 포함한 페디버스는 ActivityPub 프로토콜을 이용해 서로 통신을 합니다. `/.well-known/host-meta` 엔드포인트를 이용해 웹핑거 템플릿을 얻고 그를 이용해 웹핑거 요청을 해, 해당 유저의 정보, 관련 엔드포인트들을 얻어 서로 통신을 합니다. 저는 이 중에서 webfinger 요청을 보내고 `application/activity+json` 타입의 항목이 있는지 검사하겠습니다.

```json
{
    "aliases": [
        "https://qdon.space/@qdon_support",
        "https://qdon.space/users/qdon_support"
    ],
    "links": [
        {
            "href": "https://qdon.space/@qdon_support",
            "rel": "http://webfinger.net/rel/profile-page",
            "type": "text/html"
        },
        {
            "href": "https://qdon.space/users/qdon_support.atom",
            "rel": "http://schemas.google.com/g/2010#updates-from",
            "type": "application/atom+xml"
        },
        {
            "href": "https://qdon.space/users/qdon_support",
            "rel": "self",
            "type": "application/activity+json"
        },
        {
            "href": "https://qdon.space/api/salmon/1",
            "rel": "salmon"
        },
        {
            "href": "data:application/magic-public-key,RSA.59oXEbpg8G-fjBgcDDAFy4O05tLV01C_iKZElt0tVYd43EAispDbv3Ka4Zd1cfjOf-ATdyhE6FBmiMaeLn_-m7yrPR25jJLudEiKI1RSD3QjdYKvBc3eU5YPVHCgMjsgEC8zpC6Yr2nCK1DoPlhQXJiPPybUSsEVojfu5eHfFr47IbBUOxN8Y2ymOkLhSfP6FjszdxYMyMKMtCs677bQ0xYQf3FqHiO2KQ9IihPW9YTB9d2q-wv1RQMQZiFvEzUmzbIg1V7SmJrviKg_BAc2SL3Vt33JpgBMVclkGMn_stoLzVzAHrXQ5dIZzfj5cMsd8nkzpTLftUVffokinkUwFQ==.AQAB",
            "rel": "magic-public-key"
        },
        {
            "rel": "http://ostatus.org/schema/1.0/subscribe",
            "template": "https://qdon.space/authorize_interaction?uri={uri}"
        }
    ],
    "subject": "acct:qdon_support@qdon.space"
}
```



- url: `/.well-known/webfinger?resource=acct:qdon_support@qdon.space`
- keyword: `application/activity+json`


## 상태 페이지

소규모 서버에서 상태 페이지를 따로 구축하는 건 위험이 따릅니다. 상태 페이지가 죽을 확률도 꽤나 크기 때문이죠. 그렇기 때문에 저는 uptimerobot이라는 서비스를 사용해 위의 엔드포인트와 키워드를 설정해 넣었습니다. 커스텀 도메인도 지원하기 때문에 상태 페이지를 서브도메인에 호스팅 할 수도 있습니다. [이렇게요](https://status.qdon.space)
