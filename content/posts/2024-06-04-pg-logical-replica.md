---
title: Postgres logical 레플리카 설정
date: 2024-06-04 15:23:25 +0900
authors:
- jarm
categories:
- tech
tags:
- postgres
- sysop
---

이전에 우리는 Postgres의 Physical 레플리카 설정에 대해 알아보았습니다. 이번에는 Logical 레플리카 설정에 대해 알아보겠습니다.
<!--more-->

## 레플리카 구성의 차이

Physical 레플리카와 Logical 레플리카(이하 물리, 논리로 표기)가 각각 무엇인지부터 알아봅시다.

### 물리 레플리카

물리 레플리카는 postgres의 데이터베이스를 파일(혹은 블록) 단위로 복사하는 방식입니다. 이 방식은 원본 데이터베이스와 복제본이 완벽히 동일한 데이터를 가지게 되는 장점이 있지만 인덱스 재구성, VACUUM 등의 작업이 복제본에서도 그대로 전해지기 때문에 부하가 높아질 수 있습니다. 또한 레플리카의 postgres 버전이 원본과 동일해야 한다는 제약이 있습니다.

### 논리 레플리카

반면 논리 레플리카는 SQL 구문으로 적히는 논리적인 레벨의 복사가 이루어집니다. 이 방식은 물리 레플리카와 달리 레플리카의 postgres 버전이 원본과 동일하지 않아도 되며, 레플리카에서 인덱스 재구성, VACUUM 등의 작업을 수행하지 않아도 됩니다. 또한 일부 테이블만 복제하는 등의 설정이 가능합니다. 하지만 시퀀스 등의 정보는 복제되지 않으므로 주의가 필요합니다.

논리 레플리카는 버전이 동일하지 않아도 되기 때문에 클러스터를 구성하고, 순차적으로 업그레이드를 하는 방식으로 다운타임 없이  데이터베이스 업그레이드를 할 수 있습니다.


## postgres 설정

### pglogical 확장 설치

pglogical 확장을 우선 시스템에 설치애햐 합니다. postgres 버전에 맞는 시스템 패키지를 설치합니다.

```bash
sudo apt-get install postgresql-16-pglogical
```

### postgresql.conf 설정

확장을 이용하려면 설정파일을 바꿔주어야 합니다. `postgresql.conf` 파일을 열어 다음과 같이 설정합니다.

```ini
wal_level = logical
shared_preload_libraries = 'pglogical'
```

### 원격 연결 설정

원격 연결을 허용하기 위해 `pg_hba.conf` 파일을 열어 다음을 참고해 설정합니다.

```conf
# TYPE  DATABASE        USER            ADDRESS                 METHOD
host    production      backup          100.1.1.3/32            md5
host    production      backup          100.1.1.3/32            scram-sha-256
```

설정이 완료되었다면 postgres를 재시작합니다.

### 확장 추가

데이터베이스에 pglogical 확장을 추가합니다.

```sql
CREATE EXTENSION pglogical;
```


## 원본 노드 설정

### 계정 생성

복제를 위한 계정을 생성합니다.

```sql
CREATE ROLE backup WITH REPLICATION LOGIN PASSWORD 'password';
GRANT ALL PRIVILEGES ON DATABASE production TO backup;
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA production TO backup;
```

### Publication 생성

레플리카를 위한 복제 슬롯을 생성합니다.

```sql
CREATE PUBLICATION logi_rep FOR ALL TABLES;
```

## 복제 노드 설정

### Subscription 생성

복제 노드에서 원본 노드로부터 데이터를 받아오기 위한 구독을 생성합니다.

```sql
CREATE SUBSCRIPTION production_replica CONNECTION 'host=100.1.1.2 port=5432 password=password user=backup dbname=production' PUBLICATION logi_rep;
```

이후 복제가 잘 되는지 확인합니다.
