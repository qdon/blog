---
title: "Nginx로 미디어 캐시 프록시 만들기"
date: 2022-03-19T20:20:21+09:00
authors:
- jarm
categories:
- tech
tags:
- S3
---

[마스토돈 공식 가이드 문서][nginx-local-proxy]에는 nginx로 오브젝트 스토리지에 대한 리버스 프록시를 만들어서 도메인 주소를 유지하는 방법을 설명하고 있습니다.
하지만 이 문서대로 설정하면 바로 적용도 잘 안 되고 Cache buster에 관해서는 하나도 설명하고 있지 않으므로 다음과 같은 설정을 권장합니다.

캐시 버스터 관련 설정은 `Cache-Bypass: BUSTING_CACHE;`와 같은 헤더가 발견되면 캐시를 강제 새로고침 하는 것으로 가정했습니다.

```nginx
proxy_cache_path /tmp/nginx_bucket_cache levels=1:2 keys_zone=STATIC:10m inactive=7d max_size=1g;

map $http_cache_bypass $authorized_cache_bypass {
    default 0;
    BUSTING_CACHE 1;
}

server {

    server_name bucket.qdon.space;
    access_log /var/log/nginx/bucket.qdon.space-access.log;
    error_log /var/log/nginx/bucket.qdon.space-error.log;

    location = /favicon.ico {
        alias /var/www/favicon.ico;
        expires max;
    }

    # location /.well-known/acme-challenge {
    #     root /var/www/;
    # }

    location / {
        limit_except GET {
            deny all;
        }

        # Cache buster for mastodon
        proxy_cache_bypass $authorized_cache_bypass;

        proxy_set_header Connection '';
        proxy_set_header Authorization '';
        proxy_hide_header Set-Cookie;
        proxy_hide_header 'Access-Control-Allow-Origin';
        proxy_hide_header 'Access-Control-Allow-Methods';
        proxy_hide_header 'Access-Control-Allow-Headers';
        proxy_hide_header x-amz-id-2;
        proxy_hide_header x-amz-request-id;
        proxy_hide_header x-amz-meta-server-side-encryption;
        proxy_hide_header x-amz-server-side-encryption;
        proxy_hide_header x-amz-bucket-region;
        proxy_hide_header x-amzn-requestid;

        #DigitalOcean Spaces headers
        proxy_hide_header X-HW;
        proxy_hide_header x-amz-meta-s3cmd-attrs;

        proxy_ignore_headers Set-Cookie;
        proxy_intercept_errors off;

        proxy_buffering on;
        proxy_cache_valid 200 1d;
        # proxy_cache_revalidate on;
        proxy_cache STATIC;
        proxy_cache_use_stale error timeout updating http_500 http_502 http_503 http_504;
        proxy_cache_lock on;
        expires 1y;
        add_header Cache-Control public;
        # add_header 'Access-Control-Allow-Origin' *;
        add_header X-Cached $upstream_cache_status;

        resolver 8.8.8.8;
        set $s3_backend 'https://CHANGE_HERE!!!';
        proxy_pass $s3_backend;
        # proxy_pass_header Server;

        add_header Access-Control-Allow-Origin https://qdon.space;

        # add_header Cache-Control "public, max-age: 86400";
        # return 302 https://qdon.sfo2.cdn.digitaloceanspaces.com$request_uri;
    }


    listen [::]:443 ssl ipv6only=on;
    listen 443 ssl;

}
```

[nginx-local-proxy]: https://docs.joinmastodon.org/admin/optional/object-storage-proxy/
