# Qdon blog

[![Netlify Status](https://api.netlify.com/api/v1/badges/ac29be4c-1c7f-4200-98f5-e740cb3c8653/deploy-status)](https://app.netlify.com/sites/qdon-blog/deploys)

## Build

- install hugo
- run `hugo server -D` to start server with drafts
- run `hugo` to build (build files are in "public" directory)


## Write new post

- run `hugo new posts/2019-04-05-post-title.md`
- edit `content/posts/2019-04-05-post-title.md`

```markdown
---
title: "edit this"
date: 2019-04-05T00:00:00 +0900
category: announcement
draft: false
tags:
- qdon
---

Header summary
<!--more-->
post content
```
